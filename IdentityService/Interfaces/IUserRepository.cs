﻿using IdentityService.Domain.Core;
using System.Threading.Tasks;

namespace IdentityService.Domain.Interfaces
{
    public interface IUserRepository
    {
        User GetUserByEmail(string email);

        User GetUserById(string Id);

        Task UpdateUserAsync(User u);

        void CreateUser(User u);

        Task CreateUserAsync(User u);

        void UpdateUser(User u);

        void DeleteUser(User u);

        void Save();

        bool IsUserExsist(string Email);

    }
}
