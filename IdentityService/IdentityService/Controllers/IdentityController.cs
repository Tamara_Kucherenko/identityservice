﻿using IdentityService.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Models;
using System.Threading.Tasks;

namespace IdentityService.Controllers
{
    [Route("/api/oauth")]
    [AllowAnonymous]
    [ApiController]
    public class IdentityController : ControllerBase
    {
        ILogIn logInService;
        ISignUp signUpService;
        IRefreshTokenService refreshTokenService;
        public IdentityController(ILogIn logInService, 
            ISignUp signInService,
            IRefreshTokenService refreshTokenService)
        {
            this.logInService = logInService;
            this.signUpService = signInService;
            this.refreshTokenService = refreshTokenService;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("sign-up")]
        // route - /api/oauth/sign-up
        ///<summary>
        /// Регистрация пользователя
        ///</summary>
        public async Task<IActionResult> SignIn([FromBody] AuthenticateRequestModel model)
        {
            var tokens =  await signUpService.SignUp(model);
            if (tokens == null)
            {
                return new BadRequestObjectResult($"User with this email {model.Email} is already exist");
            }
            return new OkObjectResult(tokens);
        }

        [HttpPost]
        [Route("log-in")]
        // route - /api/oauth/log-in
        ///<summary>
        /// Вход в систему пользователя
        ///</summary>
        public async Task<IActionResult> LogIn([FromBody] AuthenticateRequestModel model)
        {
            var tokens = await logInService.LogIn(model);
            if (tokens == null)
            {
                return new BadRequestObjectResult($"User with this email {model.Email} does not exist");
            }
            return new OkObjectResult(tokens);
        }

        [HttpPost]
        [Route("refresh")]
        // route - /api/oauth/refresh
        public IActionResult RefreshTokens([FromBody] AuthenticateTokenPair tokens)
        {
            try
            {
                var newTokenPairs = refreshTokenService.UpdateExpiredTokens(tokens);
                return new OkObjectResult(newTokenPairs);
            }
            catch(SecurityTokenException ex)
            {
                Response.Headers.Add(ex.Message, "true");
                return BadRequest(ex.Message);
            }

        }
    }
}
