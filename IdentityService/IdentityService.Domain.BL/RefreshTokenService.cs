﻿using IdentityService.Domain.Interfaces;
using IdentityService.Services.Interfaces;
using Microsoft.IdentityModel.Tokens;
using Models;
using System.Security.Claims;

namespace IdentityService.Infrastructure.BL
{
    public class RefreshTokenService : AuthService, IRefreshTokenService
    {
        public RefreshTokenService(IUserRepository repository,
            ITokenCreatorService tokenService) 
            : base(repository, tokenService) { }


        public AuthenticateTokenPair UpdateExpiredTokens(AuthenticateTokenPair tokens)
        {
            var claims = tokenService.ValidateExpiredToken(tokens.AccessToken);
            var idOfUserFromClaims = claims.FindFirstValue(ClaimTypes.NameIdentifier);
            var emailOfuserFromClaims = claims.FindFirstValue(ClaimTypes.Email);

            var userFromDb = repository.GetUserById(idOfUserFromClaims);
            if (userFromDb != null && emailOfuserFromClaims.Equals(userFromDb.Email)
                && tokens.RefreshToken.Equals(userFromDb.RefreshToken))
            {
                var refreshToken = tokenService.CreateRefseshToken();
                userFromDb.RefreshToken = refreshToken;
                repository.UpdateUserAsync(userFromDb);

                return new AuthenticateTokenPair
                {
                    AccessToken = tokenService.CreateAccessToken(userFromDb.Id.ToString(), userFromDb.Email),
                    RefreshToken = refreshToken
                };
            }
            else
            {
                throw new SecurityTokenException("Invalid-refreshToken");
            }
        }
    }
}
