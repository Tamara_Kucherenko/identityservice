﻿using IdentityService.Domain.Core;
using IdentityService.Domain.Interfaces;
using IdentityService.Services.Interfaces;
using Models;
using System;
using System.Threading.Tasks;

namespace IdentityService.Infrastructure.BL
{
    public class RegistrationService : AuthService, ISignUp
    {
        public RegistrationService(IUserRepository repository,
            ITokenCreatorService service)
            : base(repository, service) {
        
        }

        public async Task<AuthenticateTokenPair> SignUp(AuthenticateRequestModel user)
        {
            if (!repository.IsUserExsist(user.Email))
            {
                var encodePassword = EncodeUserPassword(user.Password);
                var userId = Guid.NewGuid();
                var accessToken = tokenService.CreateAccessToken(userId.ToString(), user.Email);
                var refreshToken = tokenService.CreateRefseshToken();

                var tokens = new AuthenticateTokenPair() {
                    AccessToken = accessToken,
                    RefreshToken = refreshToken,
                };

                var newDbUser = new User() { 
                    Id = Guid.NewGuid(), 
                    Email = user.Email, 
                    PasswordSalt = encodePassword.PasswordSalt,
                    PasswordEncoded = encodePassword.PasswordEncoded.Result,
                    RefreshToken = refreshToken
                };
                await repository.CreateUserAsync(newDbUser);
                return tokens;
            }
            return null;
        }
    }
}
