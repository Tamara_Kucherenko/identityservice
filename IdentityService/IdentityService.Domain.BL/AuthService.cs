﻿using IdentityService.Domain.Interfaces;
using IdentityService.Services.Interfaces;
using Microsoft.IdentityModel.Tokens;
using Models;
using System;
using System.IO;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IdentityService.Infrastructure.BL
{
    public class AuthService
    {
        protected IUserRepository repository;
        protected ITokenCreatorService tokenService;
        public AuthService(IUserRepository repository, ITokenCreatorService tokenService)
        {
            this.tokenService = tokenService;
            this.repository = repository;
        }

        protected PasswordEncodeData EncodeUserPassword(string userPassword)
        {
            var passwordSalt = createSalt();
            var passwordWithSalt = CreatePasswordWithCurrentSalt(userPassword,passwordSalt);
            return new PasswordEncodeData { PasswordEncoded = passwordWithSalt, PasswordSalt = passwordSalt };
        }

        private byte[] createSalt()
        {
            int maxSaltLength = 32;
            var passwordSalt = new byte[maxSaltLength];
            using var random = new RNGCryptoServiceProvider();
            random.GetNonZeroBytes(passwordSalt);
            return passwordSalt;
        }

        protected Task<byte[]> CreatePasswordWithCurrentSalt(string password, byte[] passwordSalt)
        {
            string passwordWithSalt = password + Convert.ToBase64String(passwordSalt);
            var saltPassword = Encoding.UTF8.GetBytes(passwordWithSalt);
            using var sha = SHA256.Create();
            Stream saltPasswordStream = new MemoryStream(saltPassword);
            return sha.ComputeHashAsync(saltPasswordStream);
        }
    }
}
