﻿
using IdentityService.Domain.Core;
using IdentityService.Infrastructure.Data;
using IdentityService.Services.Interfaces;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace IdentityService.Infrastructure.BL
{
    public class TokenCreatorService : ITokenCreatorService
    {
        private readonly IOptions<TokenCreationOptions> tokenOptions;

        public TokenCreatorService(IOptions<TokenCreationOptions> options)
        {
            tokenOptions = options;
        }

        public string CreateAccessToken(string userId,string email)
        {
            var currentDateTime = DateTime.UtcNow;
            var accessJWT = new JwtSecurityToken(
                    issuer: tokenOptions.Value.Issuer,
                    audience: tokenOptions.Value.Audience,
                    notBefore: currentDateTime,
                    claims: getClaims(userId, email),
                    expires: currentDateTime.AddMinutes(tokenOptions.Value.Lifetime),
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(
                        Encoding.ASCII.GetBytes(tokenOptions.Value.Key)), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(accessJWT);
            return encodedJwt;
        }

        public string CreateRefseshToken()
        {
            var randomNumber = new byte[64];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        public ClaimsPrincipal ValidateExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = true, //you might want to validate the audience and issuer depending on your use case
                ValidAudience = tokenOptions.Value.Audience,
                ValidateIssuer = true,
                ValidIssuer = tokenOptions.Value.Issuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenOptions.Value.Key)),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid-token");
            return principal;

        }

        private List<Claim> getClaims(string userId, string email)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, userId),
                new Claim(ClaimTypes.Email, email)
            };
            return claims;
        }
    }
}
