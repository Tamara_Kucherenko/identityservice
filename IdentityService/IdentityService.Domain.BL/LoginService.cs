﻿using IdentityService.Domain.Interfaces;
using IdentityService.Services.Interfaces;
using Models;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityService.Infrastructure.BL
{
    public class LoginService : AuthService, ILogIn
    {
        public LoginService(IUserRepository repository ,
            ITokenCreatorService service) 
            : base(repository, service) { }

        public async Task<AuthenticateTokenPair> LogIn(AuthenticateRequestModel model)
        {
            var userWithEnteredEmail = repository.GetUserByEmail(model.Email);
            if(await IsPasswordCorrect(model.Password, userWithEnteredEmail.PasswordSalt, userWithEnteredEmail.PasswordEncoded))
            {
                var accessToken = tokenService.CreateAccessToken(userWithEnteredEmail.Id.ToString(), userWithEnteredEmail.Email);
                var refreshToken = userWithEnteredEmail.RefreshToken;

                return new AuthenticateTokenPair()
                {
                    AccessToken = accessToken,
                    RefreshToken = refreshToken,
                };
            }
            return null;
        }

        private async Task<bool> IsPasswordCorrect(string enteredPassword, byte[] passwordSalt, byte[] dbUserPassword)
        {
            var enteredPasswordEncoded = await CreatePasswordWithCurrentSalt(enteredPassword, passwordSalt);
            if (enteredPasswordEncoded.SequenceEqual(dbUserPassword) && (dbUserPassword?.Length != 0))
                return true;
            return false;
        }
    }
}
