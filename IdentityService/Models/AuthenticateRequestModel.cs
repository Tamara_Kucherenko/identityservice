﻿using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class AuthenticateRequestModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [RegularExpression(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,}$")]
        public string Password { get; set; }
    }
}
