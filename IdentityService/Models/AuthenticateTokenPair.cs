﻿using System;
using System.Text.Json.Serialization;

namespace Models
{
    public class AuthenticateTokenPair
    {
        /// <summary>
        /// Jwt token for access
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// Token for refresh access
        /// </summary>
        public string RefreshToken { get; set; }
    }
}
