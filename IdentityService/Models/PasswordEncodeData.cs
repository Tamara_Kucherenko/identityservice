﻿
using System.Threading.Tasks;

namespace Models
{
    public class PasswordEncodeData
    {
        public Task<byte[]> PasswordEncoded { get; set; }

        public byte[] PasswordSalt { get; set; }
    }
}
