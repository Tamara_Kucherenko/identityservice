﻿using Models;

namespace IdentityService.Services.Interfaces
{
    public interface IRefreshTokenService
    {
        AuthenticateTokenPair UpdateExpiredTokens(AuthenticateTokenPair tokens);
    }
}