﻿using Models;
using System.Threading.Tasks;

namespace IdentityService.Services.Interfaces
{
    public interface ILogIn
    {
        Task<AuthenticateTokenPair> LogIn(AuthenticateRequestModel model);
    }
}
