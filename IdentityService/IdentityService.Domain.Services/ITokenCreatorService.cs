﻿using System.Security.Claims;

namespace IdentityService.Services.Interfaces
{
    public interface ITokenCreatorService
    {
        string CreateAccessToken(string userId,string email);

        string CreateRefseshToken();

        ClaimsPrincipal ValidateExpiredToken(string token);
    }
}
