﻿using Models;
using System.Threading.Tasks;

namespace IdentityService.Services.Interfaces
{
    public interface ISignUp
    {
        Task<AuthenticateTokenPair> SignUp(AuthenticateRequestModel user);
    }
}
