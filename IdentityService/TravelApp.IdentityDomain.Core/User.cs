﻿using System;

namespace IdentityService.Domain.Core
{
    public class User
    {
        public Guid Id { get; set; }

        public string Email { get; set; }

        public byte[] PasswordEncoded { get; set; }

        public byte[] PasswordSalt { get; set; }

        public string RefreshToken { get; set; }
    }
}
