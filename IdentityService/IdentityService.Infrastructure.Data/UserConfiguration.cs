﻿using IdentityService.Domain.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IdentityService.Infrastructure.Data
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id);
            builder.Property(p => p.Email)
              .HasMaxLength(70)
              .IsRequired();
            builder.HasIndex(i => i.Email)
                .IsUnique();
            builder.Property(p => p.PasswordEncoded)
                .IsRequired();
            builder.Property(p => p.PasswordSalt)
                .IsRequired();
        }
    }
}