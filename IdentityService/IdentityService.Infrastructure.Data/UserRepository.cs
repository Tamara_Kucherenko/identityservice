﻿using IdentityService.Domain.Core;
using IdentityService.Domain.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityService.Infrastructure.Data
{
    public class UserRepository : IUserRepository
    {
        private UserContext db;

        public UserRepository (UserContext context)
        {
            db = context;
        }

        public async Task CreateUserAsync(User u)
        {
            await db.Users.AddAsync(u);
            await db.SaveChangesAsync();
        }

        public void CreateUser(User u)
        {
            db.Users.Add(u);
            db.SaveChanges();
        }

        public bool IsUserExsist(string Email)
        {
            var isUserExsist = db.Users.Any(u => u.Email.Equals(Email));
            return isUserExsist;        
        }

        public void DeleteUser(User u)
        {
            throw new System.NotImplementedException();
        }

        public User GetUserByEmail(string email)
        {
            var user = db.Users.FirstOrDefault(u => u.Email.Equals(email));
            return user;
        }

        public void Save()
        {
            throw new System.NotImplementedException();
        }

        public void UpdateUser(User u)
        {
            throw new System.NotImplementedException();
        }

        public async Task UpdateUserAsync(User u)
        {
            db.Users.Update(u);
            await db.SaveChangesAsync();
        }
        public User GetUserById(string Id)
        {
            var idGuid = new Guid(Id);
            return db.Users.Find(idGuid);
        }
    }
}
